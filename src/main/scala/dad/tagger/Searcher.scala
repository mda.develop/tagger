package dad.tagger

import dad.tagger.Entity.{SearchResult, WordIndex}
import dad.tagger.SearcherIndex.Item

import scala.collection.mutable.ArrayBuffer

class Searcher(config: Configuration) {

  private val index = SearcherIndex(config)

  def process(textIndex: Array[WordIndex]): Array[SearchResult] = {
    implicit val results = ArrayBuffer.empty[SearchResult]
    implicit val buffer = ArrayBuffer.empty[Item]
    for (word <- textIndex) {
      for (i <- (0 until buffer.size)) {
        buffer(i).next.get(word.wordId).foreach(processItem)
      }
      index.get(word.wordId).foreach(processItem)
    }
    results.toArray
  }

  private def processItem(item: Item)(implicit results: ArrayBuffer[SearchResult], buffer: ArrayBuffer[Item]): Unit = {
    if (item.termIds.nonEmpty) {
      results.append(SearchResult(item.termIds, getTermWordIds(item)))
    }
    buffer.append(item)
  }

  private def getTermWordIds(item: Item): Array[Int] = {
    val array = Array.fill(item.present)(0)
    var cur = item
    for (i <- (0 until array.length)) {
      array(array.length - 1 - i) = cur.wordId
      cur = cur.last
    }
    array
  }

}
