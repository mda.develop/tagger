package dad.tagger

import dad.tagger.Entity.{MatchResult, SearchResult, WordIndex}
import dad.tagger.Matcher.Matching

object Matcher {
  case class Matching(termIndexPos: Int, textPos: Int)
}
class Matcher(config: Configuration) {
  val matcherTermWithRandomOrder = new MatcherTermWithRandomOrder()
  val matcherTermWithDirectOrder = new MatcherTermWithDirectOrder()

  def process(results: Array[SearchResult], _textIndex: Array[WordIndex]): Array[MatchResult] = {
    val textIndex = _textIndex.map(e => (e.wordId, e)).toMap
    results
      .flatMap(result => {
        val matchings = getMatchings(result.sortedTermWordIds, textIndex)
        result.termIds
          .map(config.terms.apply)
          .flatMap(term => {
            if (term.params.randomOrder) {
              matcherTermWithRandomOrder.process(term, matchings)
            } else {
              matcherTermWithDirectOrder.process(term, matchings)
            }
          })
      })
  }

  private def getMatchings(sortedTermWordIds: Array[Int], textIndex: Map[Int, WordIndex]): Array[Matching] = {
    sortedTermWordIds.zipWithIndex
      .flatMap { case (wordId, i) => textIndex(wordId).positions.map(Matching(i, _)) }
      .sortBy(_.textPos)
  }
}
