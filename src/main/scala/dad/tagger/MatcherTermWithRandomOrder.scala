package dad.tagger

import dad.tagger.Entity.{Term, MatchResult}
import dad.tagger.Matcher.Matching
import scala.collection.mutable.ArrayBuffer

class MatcherTermWithRandomOrder {

  case class Context(term: Term, matchings: Array[Matching], from: Int, var to: Int, var matchCnt: Int = 0, var skipCnt: Int = 0)

  def process(term: Term, matchings: Array[Matching]): Array[MatchResult] = {
    val buffer = ArrayBuffer.empty[MatchResult]
    for (i <- (0 until matchings.length)) {
      val ctx = Context(term, matchings, matchings(i).textPos, matchings(i).textPos + 1)
      if (processMatch(i, ctx)) {
        buffer.append(MatchResult(term.id, ctx.from, ctx.to))
      }
    }
    buffer.toArray
  }

  private def processMatch(i: Int, ctx: Context): Boolean = {
    val matchCnt = Array.fill(ctx.term.index.length)(0)
    for (k <- (i until ctx.matchings.length)) {
      val matching = ctx.matchings(k)
      if (matching.textPos - ctx.from - ctx.matchCnt - ctx.skipCnt > ctx.term.params.contact) {
        return false
      }
      val termIndexPos = matching.termIndexPos
      val wordId = ctx.term.wordIds(termIndexPos)
      val indexPos = ctx.term.index.indexWhere(_.wordId == wordId)
      if (matchCnt(indexPos) < ctx.term.index(indexPos).positions.length) {
        matchCnt(indexPos) += 1
        ctx.to = matching.textPos + 1
        ctx.matchCnt += 1
        if (ctx.matchCnt == ctx.term.wordIds.length) {
          return true
        }
      } else if (ctx.term.params.ignoreRepeat) {
        ctx.skipCnt += 1
      }
    }
    false
  }

}
