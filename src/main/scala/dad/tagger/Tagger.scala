package dad.tagger

import dad.tagger.Entity.Tag

object Tagger {}
class Tagger(config: Configuration) {
  private val searcher = new Searcher(config)
  private val matcher = new Matcher(config)

  def process(text: Array[String]): Array[Tag] = {
    val textIndex = config.words.getIndex(text)
    val searchResults = searcher.process(textIndex)
    val matchResults = matcher.process(searchResults, textIndex)
    matchResults.map(mr => {
      val tag = config.terms(mr.termId)
      val notion = config.notions(tag.notionId)
      Tag(text, mr.textPosFrom, mr.textPosTo, tag, notion)
    })
  }

}
